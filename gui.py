import sys

from PySide6.QtCore import Qt, QAbstractTableModel, QRect
from PySide6.QtGui import QColor, QMouseEvent, QPainter
from PySide6.QtWidgets import (
    QAbstractItemView,
    QApplication,
    QHeaderView,
    QMainWindow,
    QTableView,
)

COLS = 25
ROWS = 20


class DummyView(QTableView):
    """
    Specilized QTableView capturing cursor position
    """

    def __init__(self):
        super(DummyView, self).__init__()
        # Enable mouse tracking
        self.setMouseTracking(True)
        self.viewport().setMouseTracking(True)
        # Set specialized headers views that will allow background color change
        self.setVerticalHeader(DummyHeaderView(Qt.Orientation.Vertical))
        self.setHorizontalHeader(DummyHeaderView(Qt.Orientation.Horizontal))

    def mouseMoveEvent(self, event: QMouseEvent):
        """
        Actual mouse movment event
        Updates the model with currently hover'd row/col
        """
        index = self.indexAt(event.position().toPoint())
        row = index.row()
        column = index.column()
        self.model().setCurrentHeader(row, column)


class DummyHeaderView(QHeaderView):
    """
    Specilized QHeaderView that change its background color according to what the model requires
    """

    def __init__(self, orientation: Qt.Orientation, parent=None):
        super(DummyHeaderView, self).__init__(orientation)

    def paintSection(self, painter: QPainter, rect: QRect, logicalIndex: int):
        """
        Paint method that is triggered when the model emit 'headerDataChanged'
        """
        # Query the model the header cell BackgroundRole
        bg = self.model().headerData(
            logicalIndex, self.orientation(), Qt.BackgroundRole
        )
        if bg is not None:
            painter.save()
            # Do what the standard implementation would do
            super(DummyHeaderView, self).paintSection(painter, rect, logicalIndex)
            painter.restore()
            # Force background color
            painter.setCompositionMode(QPainter.CompositionMode_Multiply)
            painter.fillRect(rect, bg)
        else:
            super(DummyHeaderView, self).paintSection(painter, rect, logicalIndex)


class DummyModel(QAbstractTableModel):
    """
    This is yout usual Qt TableModel
    For the whole thing to work one need to add the part flagged with 'NEEDED'
    """

    def __init__(self):
        super(DummyModel, self).__init__()
        self.current_column = -1  # NEEDED
        self.current_row = -1  # NEEDED

    def data(self, index, role):
        if role == Qt.BackgroundRole:
            return QColor.fromRgb(0xCC, 0xCC, 0xCC)

    def rowCount(self, parent) -> int:
        return ROWS

    def columnCount(self, parent) -> int:
        return COLS

    def headerData(self, section: int, orientation: Qt.Orientation, role: int):
        if role == Qt.DisplayRole:
            if orientation == Qt.Orientation.Horizontal:
                return f"C{section}"
            else:
                return f"R{section}"
        if (
            role == Qt.BackgroundRole
        ):  # NEEDED This part would not work consistently without an specilized QTableView
            if (
                orientation == Qt.Orientation.Horizontal
                and section == self.current_column
            ) or (
                orientation == Qt.Orientation.Vertical and section == self.current_row
            ):
                return QColor.fromRgb(0xB4, 0xD4, 0xDF, 120)

    def setCurrentHeader(self, row, column):
        """
        NEEDED
        Triggers a redraw of the headers when called
        """
        if self.current_column != column:
            self.current_column = column
            self.headerDataChanged.emit(Qt.Orientation.Horizontal, 0, ROWS)
        if self.current_row != row:
            self.current_row = row
            self.headerDataChanged.emit(Qt.Orientation.Vertical, 0, COLS)


class DummyApp(QApplication):
    def __init__(self):
        super(DummyApp, self).__init__(sys.argv)
        self.win = QMainWindow()
        self.model = DummyModel()
        self.table = DummyView()
        self.init_ui()

    def init_ui(self):
        self.win.setMinimumSize(900, 500)
        self.win.setWindowTitle("Example app")
        self.win.statusBar().setMaximumHeight(20)

        self.table.setModel(self.model)
        self.table.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        self.table.setSelectionMode(QAbstractItemView.NoSelection)

        self.win.setCentralWidget(self.table)

    def show(self):
        self.win.show()


if __name__ == "__main__":
    app = DummyApp()
    app.show()
    sys.exit(app.exec())

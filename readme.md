# QTableView easy on the eyes

This project shows a simple overload in the default Qt TableView that helps identify which row and column the user is currently hovering with the cursor by changing the header backgroud color.

![screenshot](screenshot.png "screenshot")

Note : on some OSes this behaviour cannot be achieved by simply using the model `Qt.BackgroundRole`

# How to use ?

This project is not packaged as a library. Read the `gui.py` you'll see it's fairly simple and the parts you'll need to copy are marked with the `NEEDED` comment. 
